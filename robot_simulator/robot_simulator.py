import sys, os, pygame

#funcion para machear las posiciones de los objetos en la pantalla
def match_position(tup):
    field0 = (0,0)
    field1 = (100,0)
    field2 = (200,0)
    field3 = (300,0)

    field4 = (0,100)
    field5 = (100,100)
    field6 = (200,100)
    field7 = (300,100)

    field8 = (0,200)
    field9 = (100,200)
    field10 = (200,200)
    field11 = (300,200)

    field12 = (0,300)
    field13 = (100,300)
    field14 = (200,300)
    field15 = (300,300)
    
    if tup[0] == 0:
        if tup[1] == 0:
            return field0
        if tup[1] == 1:
            return field1
        if tup[1] == 2:
            return field2
        if tup[1] == 3:
            return field3
    if tup[0] == 1:
        if tup[1] == 0:
            return field4
        if tup[1] == 1:
            return field5
        if tup[1] == 2:
            return field6
        if tup[1] == 3:
            return field7
    if tup[0] == 2:
        if tup[1] == 0:
            return field8
        if tup[1] == 1:
            return field9
        if tup[1] == 2:
            return field10
        if tup[1] == 3:
            return field11
    if tup[0] == 3:
        if tup[1] == 0:
            return field12
        if tup[1] == 1:
            return field13
        if tup[1] == 2:
            return field14
        if tup[1] == 3:
            return field15

def simulate(result):
    initial = None
    events = []
    machines = []

    for action, state in result.path():
        if action is None:
            initial = state
        else:
            events.append(action)

    pygame.init()
    pygame.font.init()

    screen = pygame.display.set_mode((400,400))

    background = pygame.image.load(os.path.join('robot_simulator', 'background.png'))
    screen.blit(background, (0,0))

    #cargo los objetos en la pantalla
    for index, object in enumerate(initial):
        if index == 0:
            robot = pygame.image.load(os.path.join('robot_simulator', 'robot.png'))
            screen.blit(robot, match_position(object))
        else:
            machine = pygame.image.load(os.path.join('robot_simulator', 'machine.png'))
            screen.blit(machine, match_position(object))
            machines.append(match_position(object))


    pygame.display.update()
    pygame.time.delay(1000)

    #recorro la lista de eventos para generar los movimientos en la pantalla
    for event in events:
        screen.blit(background,(0,0))
        if event[0] == -1:
            #it means 'extint'
            extinguisher = pygame.image.load(os.path.join('robot_simulator', 'extinguisher.png'))
            for position in machines:
                screen.blit(machine, position)
            screen.blit(robot, match_position((event[1], event[2])))
            screen.blit(extinguisher, (match_position((event[1], event[2]))[0]+60, match_position((event[1], event[2]))[1]+12))
            pygame.display.update()

            pygame.time.delay(1000)

            screen.blit(background,(0,0))
            for position in machines:
                screen.blit(machine, position)
            screen.blit(robot, match_position((event[1], event[2])))

        elif event[0] == 0:
            #it means 'move'
            for position in machines:
                screen.blit(machine, position)
            screen.blit(robot, match_position((event[1], event[2])))

        else:
            #it means 'push that machine'
            machine_number = event[0] - 1
            machines[machine_number] = match_position((event[1], event[2]))
            for position in machines:
                screen.blit(machine, position)
            screen.blit(robot, match_position((event[1], event[2])))

        pygame.display.update()
        pygame.time.delay(1000)

    label = pygame.image.load(os.path.join('robot_simulator', 'finished.png'))
    screen.blit(label, (0,150))
    pygame.display.update()

    pygame.time.delay(2000)